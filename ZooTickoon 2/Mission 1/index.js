 // Script pour le carousel
 let slideIndex = 0;
 const slide = document.querySelector('.carousel-slide');

 function move(n) {
     slideIndex += n;
     const totalSlides = slide.childElementCount;

     if (slideIndex < 0) {
         slideIndex = totalSlides - 1;
     } else if (slideIndex >= totalSlides) {
         slideIndex = 0;
     }

     slide.style.transform = `translateX(-${slideIndex * 300}px)`;
 }